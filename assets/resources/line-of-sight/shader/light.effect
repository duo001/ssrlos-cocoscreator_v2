CCEffect %{
  techniques:
  - passes:
    - vert: vs
      frag: fs
      blendState:
        targets:
        - blend: true
      rasterizerState:
        cullMode: none
      properties:
        radius: { value: 0.1 }
        sourceRadius: { value: 2.0 }
        intensity: { value: 0.8 }
        center: { value: [0.0, 0.0] }
}%


CCProgram vs %{
  precision highp float;

  #include <cc-global>
  #include <cc-local>

  in vec3 a_position;
  in vec4 a_color;
  out vec4 v_color;

  void main () {
    vec4 pos = vec4(a_position, 1);

    #if CC_USE_MODEL
    pos = cc_matViewProj * cc_matWorld * pos;
    #else
    pos = cc_matViewProj * pos;
    #endif

    v_color = a_color;

    gl_Position = pos;
  }
}%


CCProgram fs %{
  precision highp float;

  in vec4 v_color;

  uniform params {  
    vec2 center;
    float radius;
    float sourceRadius;
    float intensity;
  };

  float circleDist(vec2 p, float radius) {
    return length(p) - radius;
  }
  
  float fillMask(float dist) {
    return clamp(-dist, 0.0, 1.0);
  }
  
  float shadow(vec2 p, vec2 pos, float radius) {
     vec2 dir = normalize(pos - p);
     float dl = length(p - pos);
     float lf = radius * dl;
     float dt = 0.01;
     lf = clamp((lf*dl + radius) / (2.0 * radius), 0.0, 1.0);
     lf = smoothstep(0.0, 1.0, lf);
     return lf;
  }
  
  vec4 drawLight(vec2 p, vec2 pos, vec4 color, float range, float radius) {
     float ld = length(p - pos);
     if (ld > range) return vec4(0.0);
     float shad = shadow(p, pos, radius);
     float fall = (range - ld)/range;
     fall *= fall;
     float source = fillMask(circleDist(p - pos, radius));
     return (shad * fall + source) * color;
  }

  float luminance(vec4 col) {
    return 0.2126 * col.r + 0.7152 * col.g + 0.0722 * col.b;
  }

  void setLuminance(inout vec4 col, float lum) {
     lum /= luminance(col);
     col *= lum;
  }

  void main () {
    vec2 p = gl_FragCoord.xy;
    vec4 lightCol = v_color;
    setLuminance(lightCol, intensity);
    vec4 col = drawLight(p, center, lightCol, radius, sourceRadius);
    gl_FragColor = clamp(col, 0.0, 1.0);
  }
}%
