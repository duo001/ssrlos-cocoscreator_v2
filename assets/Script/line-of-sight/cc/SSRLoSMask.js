/****************************************************************************
 Copyright (c) 2017-2018 SuperSuRaccoon
 
 Site: http://www.supersuraccoon-cocos2d.com
 Mail: supersuraccoon@gmail.com

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 ****************************************************************************/
 
const ssr = require('../namespace/SSRLoSNamespace');
 
/**
 * @classdesc The component for rendering sight area with mask effect (using cc.ClippingNode).
    JSBinding Support [○]
    JSBinding Functions Support:
        addTarget
        removeTarget
        removeTargets
        removeAllTargets
        updateTarget
        updateTargets
 * @class
 * @extends cc.Node
 * @prop {cc.LayerColor}                       maskLayer                        - A color layer for mask effect.
 * @prop {cc.Node}                             maskStencilNode                  - Mask stencilNode container.
 * @prop {cc.ClippingNode}                     maskClipper                      - The cc.ClippingNode instance.
 * @prop {Array.<cc.Node>}                     targets                          - The targets whoses sight area will be rendered for mask effect.
 * @prop {Array.<losComponentCoreProvider>}    losComponentCoreProviders        - Functions for providing ssr.LoS.Component.Core instance for each target node.
 * @prop {Array.<cc.DrawNode>}                 maskRenders                      - The stencil render, one per target node.
 */
ssr.LoS.Mask = cc.Class( /** @lends ssr.LoS.Component.Mask# */ {
    name: "ssr.LoS.Mask",
    "extends": cc.Object,
    /**
     * The constructor
     * @function
     */
    ctor:function() {
        this._mask = arguments[0];
        //
        this._targets = [];
    },
    /**
     * Add a target whose sight area will be rendered for mask effect.
     * @function
     * @param {cc.Node} node The target to be added.
     */
    addTarget:function(node, losComponentCoreProvider) {
        var result = this.findTarget(node);
        if (result != -1) {
            cc.log("ssr.LoS.Component.Mask addTarget node already added");
        }
        else {
            this._targets.push(node);
        }
    },
    /**
     * Remove a target.
     * @function
     * @param {cc.Node} node The target to be removed.
     * @return {Boolean} True for removed false for target not found.
     */
    removeTarget:function(node) {
        for (var i = 0, l = this._targets.length; i < l; i ++) {
            if (node === this._targets[i]) {
                this._targets.splice(i, 1);
                return true;
            }
        } 
        return false;
    },
    /**
     * Remove all the given targets.
     * @function
     * @param {Array.<cc.Node>} nodes The targets to be removed.
     */
    removeTargets:function(nodes) {
        for (var i = 0, l = nodes.length; i < l; i ++) {
            this.removeTarget(nodes[i]);
        } 
    },
    /**
     * Remove all the targets.
     * @function
     */
    removeAllTargets:function() {
        for (var i = 0, l = this._targets.length; i < l; i ++) {
            this.removeTarget(this._targets[i]);
        }
        this._targets = [];
    },
    /**
     * Find if the given target is already added.
     * @function
     * @param {cc.Node} node The target to be removed.
     * @return {Number} Index of the node. -1 for not found
     */
    findTarget:function(node) {
        for (var i = 0, l = this._targets.length; i < l; i ++) {
            if (node === this._targets[i]) {
                return i;
            }
        }
        return -1;
    },
    /**
     * Update the target's sight area mask effect.
     * @function
     * @param {cc.Node} node The target whose sight area to be updated.
     * @param {Boolean} [needForceUpdate=false] If need to update the target's sight area no matter it is changed or not.
     */
    updateTarget:function(node, needForceUpdate) {
        var nodeIndex = this.findTarget(node);
        if (nodeIndex == -1) {
            cc.log("ssr.LoS.Component.Mask updateTarget node is not added!")
        }
        needForceUpdate = (needForceUpdate === undefined ? false : needForceUpdate);
        var losCore = node.getComponent("SSRLoSComponentCore").getLoSCore();
        ssr.LoS.Render.Util.renderSightArea(losCore, this._mask._graphics);
        // this._mask.node.getChildren()[0].position = losCore._getCameraScreenPosition();
        // this._mask.node.getChildren()[0].position = cc.Camera.findCamera(losCore.getOwner()).getScreenToWorldPoint(cc.v2(0, 0));
    },
    /**
     * Update the targets' sight area mask effect.
     * @function
     * @param {Array.<cc.Node>} nodes The targets whose sight area to be updated.
     * @param {Boolean} [needForceUpdate=false] If need to update the target's sight area no matter it is changed or not.
     */
    updateTargets:function(nodes, needForceUpdate) {
        if (!nodes) {
            nodes = this._targets;
        }
        this._mask._graphics.clear();
        for (var i = 0, l = nodes.length; i < l; i ++) {
            this.updateTarget(nodes[i], needForceUpdate);
        }
    },
    /**
     * Update the targets' sight area mask effect.
     * @function
     * @param {Array.<cc.Node>} nodes The targets whose sight area to be updated.
     * @param {Boolean} [needForceUpdate=false] If need to update the target's sight area no matter it is changed or not.
     */
    isNeedUpdate:function(nodes) {
        if (!nodes) {
            nodes = this._targets;
        }
        var needUpdate = false;
        for (var i = 0, l = nodes.length; i < l; i ++) {
            var nodeIndex = this.findTarget(nodes[i]);
            if (nodeIndex == -1) {
                continue;
            }
            var losCore = nodes[i].getComponent("SSRLoSComponentCore").getLoSCore();
            if (losCore.isUpdated()) {
                needUpdate = true;
                break;
            }
        }
        return needUpdate;
    },
});
